/**
 * Created by Florian on 20/10/2015.
 */
var id;

var app = {
    modules: {}
};

app.modules.platData = (function () {
    return {

        /*
         methode qui structure l'affichage de tout les plat
         */

        sendPlats: function (data) {
            // on vide le contenue de la div class= "lis-plat"
            $(".list-plat").empty();

            // on parcours notre  liste de plats
            data.forEach(function (e) {
                var html = '<p>' + e.nom + '</a></p>';
                var jhtml = $(html);

                $(".list-plat").append(jhtml);
                jhtml.click(function () {
                    app.modules.plats.afficherPlat(e.id);

                })
            })
        },


        /*
         methode qui structure l'affichage d'un plat
         */
        sendPlatById: function (data) {
            //mise en page du paragaphe
            var html = '<p class="plat_joli">' + 'nom: ' + data.plat.nom + '<br>' +
                'description:' + data.plat.description + '<br>' +
                'prix: ' + data.plat.prix + '<br>' +
                '<img style=width:50% src="https://webetu.iutnc.univ-lorraine.fr/www/canals5/prolunch/' +
                data.plat.photo.href + '"/>' + '<button id="add" >ajouter au panier</button>' + '</p>';
            //$('#add').click(function(){
            //    alert('coucou pas moi');
            //});
            var jhtml$ = $(html);


            id=data.plat.id;
            // on vide le contenue de  de notre div class= "lis-plat" et ajoute le nouveau contenue
            $(".list-plat").empty();
            $(".list-plat").append(jhtml$);


        }
    }


})();


app.modules.plats = (function () {


    return {

        afficherAllPlat: function () {
            $.ajax({
                    url: 'https://webetu.iutnc.univ-lorraine.fr/www/canals5/prolunch/plats',
                    type: 'GET',
                    dataType: 'json',
                    success: function (data) {
                        app.modules.platData.sendPlats(data);
                    },
                    xhrFields: {
                        withCredentials: true
                    },
                    crossDomain: true

                }
            );


        },


        afficherPlat: function (num) {


            $.ajax({
                url: 'https://webetu.iutnc.univ-lorraine.fr/www/canals5/prolunch/plats/' + num,
                type: 'GET',
                dataType: 'json',
                success: function (data) {
                    app.modules.platData.sendPlatById(data);
                },
                xhrFields: {
                    withCredentials: true
                },
                crossDomain: true

            });


        }


    }


})();

app.modules.panier = (function () {


    function article(id, nom, img, prix) {
        this.id = id;
        this.nom = nom;
        this.img = img;
        this.prix = prix;
        this.qte = 1;

    }

    var panier = new Array();
    var tab = '<table> <tr> <td> nomArticle</td> <td>qte</td> <td>Prix</td> </tr>';

    return {

        sendItemPanier: function (num) {
            $.ajax({
                url: 'https://webetu.iutnc.univ-lorraine.fr/www/canals5/prolunch/plats/' + num,
                type: 'GET',
                dataType: 'json',
                success: function (data) {
                    app.modules.panier.ajouteItemPanier(data);
                },
                xhrFields: {
                    withCredentials: true
                },
                crossDomain: true

            });


        },

        ajouteItemPanier: function (data) {



            panier.push(new article(data.plat.id, data.plat.nom, data.plat.img, data.plat.prix));



            app.modules.panier.affichePanier();
        },

        affichePanier: function () {



            panier.forEach(function(e){
                tab += '<tr></tr><td>' + e.nom + '</td>' +
                '<td>' + e.qte + '</td>' +
                '<td>' + e.prix + '</td></tr> ';

            })




            $('.panier').empty();
            $('.panier').append(tab);
        },


        supprimePanier: function (id) {
            $(".panier").empty();
        }

    }

})();

app.modules.commande=(function () {




    return{


        creerCommande: function(){
            $.ajax({
                url: 'https://webetu.iutnc.univ-lorraine.fr/www/canals5/prolunch/commande/',
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    app.modules.commande.ajouterCommande(data);
                },
                xhrFields: {
                    withCredentials: true
                },
                crossDomain: true

            });
        }





    }


})();


app.modules.init = (function () {


    return {

        init: function () {

            //methode plats
            $('#plat').click(function () {
                app.modules.plats.afficherAllPlat();
            });


           $("body").on("click","#add", function(){
                app.modules.panier.sendItemPanier(id);
           });
            $('#del').click(function () {
                app.modules.panier.supprimePanier();
            });

        }


    }

})();


$(document).ready(function () {
    app.modules.init.init();
});

